package ru.translator.ui.splash;

/**
 * Created by Ruslan on 07.06.2017.
 */

public interface SplashView {
    void onLangsLoaded();
}
